import sys
import dash
import dash_core_components as dcc
import dash_html_components as html

from dash.dependencies import Input, Output, State

from rdflib import Graph

import igraph as ig
import json
import xml.etree.ElementTree as ET

import plotly.graph_objs as go


#  Falk todo:
# - make tool fully installable for 3rd parties incl. data downloaded from iirds.org?
# - make relation type weighs configurable
# - when clickin on a topic,
# -- update graph with displaying all relations of this topic.
# -- show all labels of the related nodes
# -- center on that node (seed?)
# - maybe integrate with iirds toolkit or let user viusalize own package?

#config
#dev-only
debug_ext = True
show_data_in_console = False

#data config
default_node_types = ['Package', 'Topic']
packages = {
    'default': {
        'sel_value_packages': 'PI-FAN-OP',
        'name': 'PI-Fan X5-DH2 - PI-FAN Operating Manual',
        'rdf_file': 'test-data/iiRDS-Sample-Content-2019-10-31/iirds-sample-1/META-INF/metadata.rdf',
    },
    'PI-FAN-OP': {
        'sel_value_packages': 'PI-FAN-OP',
        'name': 'PI-Fan X5-DH2 - PI-FAN Operating Manual',
        'rdf_file': 'test-data/iiRDS-Sample-Content-2019-10-31/iirds-sample-1/META-INF/metadata.rdf'
    },
    'PI-REMOTE-OP': {
        'sel_value_packages': 'PI-REMOTE-OP',
        'name': 'PI-REMOTE TEST DATA - Operating Manual',
        'rdf_file': 'test-data/iiRDS-Sample-Content-2019-10-31-custom/iirds-sample-1/META-INF/metadata.rdf'
    }
}


def get_relations(_type, g):
    relations = []

    if _type['name'] == 'Package_to_Topic':
        res = g.query(
            '''SELECT DISTINCT ?a
                               ?b
               WHERE {
                    ?b iirds:is-part-of-package ?a .
                    ?b iirds:has-topic-type ?c
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.a)
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'Package_to_Document':
        res = g.query(
            '''SELECT DISTINCT ?a
                               ?b
               WHERE {
                    ?b iirds:is-part-of-package ?a .
                    ?b iirds:has-document-type ?doc_type
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.a)
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'Document_to_Directory_Node':
        res = g.query(
            '''SELECT DISTINCT ?a
                               ?dir_label
               WHERE {
                    ?b iirds:relates-to-information-unit ?a .
                    ?a iirds:has-document-type ?doc_type .
                    ?b rdfs:label ?dir_label
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.a)
            relation['target'] = str(row.dir_label)
            relations.append(relation)

    elif _type['name'] == 'Directory_Node_to_Directory_Node':
        res = g.query(
            '''SELECT DISTINCT ?dir_a_label
                               ?dir_b_label
               WHERE {
                    ?a iirds:has-first-child|iirds:has-next-sibling ?b .
                    ?a rdfs:label ?dir_a_label .
                    ?b rdfs:label ?dir_b_label
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.dir_a_label)
            relation['target'] = str(row.dir_b_label)
            relations.append(relation)

    elif _type['name'] == 'Directory_Node_to_Topic':
        res = g.query(
            '''SELECT DISTINCT ?dir_label
                               ?b
               WHERE {
                    ?a iirds:relates-to-information-unit ?b .
                    ?a rdfs:label ?dir_label
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.dir_label)
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'X_to_Component':
        res = g.query(
            '''SELECT DISTINCT ?a
                               ?b
               WHERE {
                    ?a iirds:relates-to-component ?b
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.a)
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'fake_Commponent_Tree':
        res = g.query(
            '''SELECT DISTINCT ?b
               WHERE {
                    ?a iirds:relates-to-component ?b .
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = 'component_root'
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'X_to_Qualification':
        res = g.query(
            '''SELECT DISTINCT ?a
                               ?b
               WHERE {
                    ?a iirds:relates-to-qualification ?b
               }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = str(row.a)
            relation['target'] = str(row.b)
            relations.append(relation)

    elif _type['name'] == 'fake_Qualification_Tree':
        res = g.query(
            '''SELECT DISTINCT ?b
               WHERE {
                    ?a iirds:relates-to-qualification ?b
                    }
            '''
        )

        for row in res:
            relation = {}
            relation['source'] = 'qualification_root'
            relation['target'] = str(row.b)
            relations.append(relation)

    if show_data_in_console:
        easy_output('got relations for ' + _type['name'], relations)

    return relations


def get_nodes(_type, metadata_tree, ns):

    nodes = []

    if _type == 'Package':
        package = metadata_tree.find('.//iirds:Package', ns)
        node = {}
        node['id'] = package.attrib.get('{' + ns['rdf'] + '}about')
        node['name'] = package.attrib.get('{' + ns['rdf'] + '}about')
        node['group'] = 2
        nodes.append(node)

    elif _type == 'Document':
        documents = metadata_tree.findall('.//iirds:Document', ns)
        for document in documents:
            node = {}
            node['id'] = document.attrib.get('{' + ns['rdf'] + '}about')
            node['name'] = document.find('./iirds:title', ns).text
            node['group'] = 4
            nodes.append(node)

    elif _type == 'Directory_Node':
        directory_nodes = metadata_tree.findall('.//iirds:DirectoryNode', ns)
        index = 0
        for directory_node in directory_nodes:
            node = {}
            node['id'] = directory_node.find('./rdfs:label', ns).text
            node['name'] = directory_node.find('./rdfs:label', ns).text
            node['group'] = 3
            nodes.append(node)
            index += 1

    elif _type == 'Topic':
        topics = metadata_tree.findall('.//iirds:Topic', ns)
        for topic in topics:
            node = {}
            node['id'] = topic.attrib.get('{' + ns['rdf'] + '}about')
            node['name'] = topic.find('iirds:title', ns).text
            node['group'] = 1
            nodes.append(node)

    elif _type == 'Component':
        components = metadata_tree.findall('.//iirds:Component', ns)
        for component in components:
            node = {}
            node['id'] = component.attrib.get('{' + ns['rdf'] + '}about')
            node['name'] = component.find('rdfs:label', ns).text
            node['group'] = 6
            nodes.append(node)

    elif _type == 'Component_root':
        node = {}
        node['id'] = 'component_root'
        node['name'] = 'Components'
        node['group'] = 9
        nodes.append(node)

    elif _type == 'Qualification':
        qualifications = metadata_tree.findall('.//iirds:Role', ns)
        for qualification in qualifications:
            node = {}
            node['id'] = qualification.attrib.get('{' + ns['rdf'] + '}about')
            node['name'] = qualification.find('rdfs:label', ns).text
            node['group'] = 7
            nodes.append(node)

    elif _type == 'Qualification_root':
        node = {}
        node['id'] = 'qualification_root'
        node['name'] = 'Qualifications'
        node['group'] = 8
        nodes.append(node)

    if show_data_in_console:
      easy_output('got nodes of type ' + _type, nodes)

    return nodes


def get_data(rdf_file, node_types):
    nodes = []

    metadata_tree = ET.parse(rdf_file)

    ns = {'iirds': 'http://iirds.tekom.de/iirds#',
          'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
          'rdfs': 'http://www.w3.org/2000/01/rdf-schema#'}

    package_id = ''
    for _type in node_types:
        _type_nodes = get_nodes(_type, metadata_tree, ns)
        if _type == 'Package':
            package_id = _type_nodes[0]['id']
        for node in _type_nodes:
            node['package'] = package_id
            node['type'] = _type
            nodes.append(node)
    for count, node in enumerate(nodes, 0):
        node['graph-id'] = count

    g = Graph()
    g.parse(rdf_file, format='xml')
    relations = []
    for _type in get_relation_types(node_types):
        _type_relations = get_relations(_type, g)
        for relation in _type_relations:
            relation['weight'] = _type['weight']
            relations.append(relation)

    links = []
    for relation in relations:
        link = {}
        for node in nodes:
            if node['id'] == relation['source']:
                link['source'] = node['graph-id']
            if node['id'] == relation['target']:
                link['target'] = node['graph-id']
            link['weight'] = relation['weight']
        link_source = link.get('source', '')
        link_target = link.get('target', '')
        if (link_source or link_source == 0) and \
           (link_target or link_target == 0):
            links.append(link)

    return nodes, links


def get_graph_iirds(rdf_files, node_types):

    nodes = []
    links = []
    offset_n = 0
    for count, rdf_file in enumerate(rdf_files):
        this_nodes, this_links = get_data(rdf_file, node_types)
        nodes = nodes + offset_nodes(this_nodes, offset_n)
        links = links + offset_links(this_links, offset_n)
        offset_n = len(this_nodes)

    labels = []
    group = []
    ids = []
    types = []
    for count, node in enumerate(nodes, 1):
        labels.append(node['name'])
        group.append(node['group'])
        ids.append(node['id'])
        types.append(node['type'])

    if len(rdf_files) > 1:
        links = links + get_matched_links(nodes)

    weights = [x['weight'] for x in links]

    if show_data_in_console:
        easy_output('nodes', nodes)
        easy_output('links', links)

    N = len(nodes)
    L = len(links)
    Edges = [(links[k]['source'],
              links[k]['target']) for k in range(L)]

    if show_data_in_console:
        easy_output('Edges', Edges)

    G = ig.Graph(Edges, directed=True)

    layt = G.layout('fr_3d', dim=3, weights=weights) # Fruchterman-Reingold layout
    # layt = G.layout('kk', dim=3) # 3D Kamada-Kawai layout
    # layt = G.layout('grid_3d', dim=3) # regular grid layout in 3D
    # layt = G.layout('drl_3d', dim=3) #3D DrL layout for large graphs
    # layt = G.layout('random_3d', dim=3)

    if show_data_in_console:
        app.logger.info('L: ' + str(L))
        app.logger.info('N: ' + str(N))
        easy_output('layt', layt)

    Xn = [layt[k][0] for k in range(N)]  # x-coordinates of nodes
    Yn = [layt[k][1] for k in range(N)]  # y-coordinates
    Zn = [layt[k][2] for k in range(N)]  # z-coordinates
    Xe = []
    Ye = []
    Ze = []
    for e in Edges:
        Xe += [layt[e[0]][0], layt[e[1]][0], None]  # x-coord of edge ends
        Ye += [layt[e[0]][1], layt[e[1]][1], None]
        Ze += [layt[e[0]][2], layt[e[1]][2], None]

    trace1 = go.Scatter3d(x=Xe,
                          y=Ye,
                          z=Ze,
                          mode='lines',
                          line=dict(color='rgb(125,125,125)', width=1),
                          hoverinfo='none',
                          )

    trace2 = go.Scatter3d(x=Xn,
                          y=Yn,
                          z=Zn,
                          mode='markers',
                          name='Family members',
                          marker=dict(symbol='circle',
                                      size=6,
                                      color=group,
                                      colorscale='Viridis',
                                      line=dict(color='rgb(50,50,50)',
                                                width=0.5)
                                      ),
                          text=labels,
                          hoverinfo='text',
                          customdata=nodes,
                          showlegend=False
                          )

    axis = dict(showbackground=False,
                showline=False,
                zeroline=False,
                showgrid=False,
                showticklabels=False,
                title=''
                )

    layout = go.Layout(# title='PI-Fan X5-DH2 - PI-FAN Operating Manual',
                       # width=1000,
                       height=600,
                       showlegend=False,
                       scene=dict(
                            xaxis=dict(axis),
                            yaxis=dict(axis),
                            zaxis=dict(axis)),
                       margin=dict(t=100),
                       clickmode='event+select',
                       hovermode='closest',
                       annotations=[
                           dict(
                               showarrow=False,
                               text='',
                               xref='paper',
                               yref='paper',
                               x=0,
                               y=0.1,
                               xanchor='left',
                               yanchor='bottom',
                               font=dict(
                                   size=14))],)
    data = [trace1, trace2]
    return go.Figure(data=data, layout=layout)


def get_about_card():
    """

    :return: A Div containing title & descriptions.
    """
    return [html.Div(
        id="about-card",
        children=[
            html.H3("Welcome to the Visual Explorer for iiRDS!"),
            html.P("Start by exploring or selecting packages and node types in the Settings tab.")
        ]
    )]


def get_display_card(display_store, memory_data):
    """
    :return: A Div for displaying iiRDS info.
    """
    customdata = ''
    content = json.loads(display_store)
    rdf_files = get_memory_values(memory_data, 'rdf_file')

    if show_data_in_console:
        app.logger.info('CONTENT ----------------')
        app.logger.info(content)
    if content:
        customdata = content['points'][0].get('customdata')
    if customdata:
        label = customdata.get('name')
        type = customdata.get('type')
        _id = customdata.get('id', 'id')
        if show_data_in_console:
            app.logger.info('label: ' + label)
        if type == 'Topic':
            topic_id = content['points'][0]['customdata']['id']
            topic_id = topic_id.split('-offset')[0]
            for rdf_file in rdf_files:
                metadata_tree = ET.parse(rdf_file)

                ns = {'iirds': 'http://iirds.tekom.de/iirds#',
                      'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#'}

                xPath_exp = f".//*[@rdf:about='{topic_id}']"
                topic = metadata_tree.find(xPath_exp, ns)
                if not topic:
                    continue
                renditions = []
                for rendition in topic.findall('.//iirds:Rendition', ns):
                    format = rendition.find('iirds:format', ns)
                    source = rendition.find('iirds:source', ns)
                    renditions.append([format.text, source.text])
                if renditions:
                    #  Falk: cleaner might be putting it into a separate dir
                    #  see https://community.plot.ly/t/html-iframe-in-dash-layout/5281
                    result = [html.Iframe(
                        src=app.get_asset_url(renditions[0][1]),
                        id='node-content-display')]

        elif label:
            result = [html.P('Label: ' + label),
                      html.P('Node type: ' + type),
                      html.P('Node id: ' + _id)]
    else:
        result = [html.P('Nothing selected to be displayed')]

    return [html.Div(
        id="display-card",
        children=result
    )]


def get_config_card(memory_data):
    """
    :return: A Div containing configs for graphs.
    """
    value_packages = get_memory_values(memory_data, 'sel_value_packages')
    value_node_types = get_memory_values(memory_data, 'sel_value_node_types')

    return [html.Div(
        id="config-card",
        children=[
            html.H3("Packages"),
            dcc.Dropdown(
                id="package",
                options=[
                    {'label': 'PI-Fan X5-DH2 - PI-FAN Operating Manual',
                     'value': 'PI-FAN-OP'},
                    {'label': 'PI-REMOTE TEST DATA - Operating Manual',
                     'value': 'PI-REMOTE-OP'}
                ],
                value=value_packages,
                multi=True
            ),
            html.H3("Node Types"),
            dcc.Dropdown(
                id="node_types",
                options=[
                    {'label': 'Topic',
                     'value': 'Topic'},
                    {'label': 'Document and Directory Node',
                     'value': 'Document_AND_Directory_Node'},
                    {'label': 'Component',
                     'value': 'Component_AND_Component_root'},
                    {'label': 'Qualification',
                     'value': 'Qualification_AND_Qualification_root'}
                ],
                value=value_node_types,
                multi=True
            )
        ]
    )]


def get_chart_card(memory_data):
    """
    :return: A Div for displaying the chart.
    """
    rdf_files = get_memory_values(memory_data, 'rdf_file')
    node_types = get_memory_values(memory_data, 'node_types')
    names = get_memory_values(memory_data, 'name')
    return html.Div(
        id="chart-card",
        children=[
            html.H2(', '.join(names)),
            dcc.Graph(
                id='network-graph',
                figure=get_graph_iirds(rdf_files, node_types)
            )
        ]
    )


def get_relation_types(node_types):

    relation_types = []
    if ('Document' not in node_types):
        relation_types.append({'name': 'Package_to_Topic',
                               'weight': 0.5})
    if 'Document' in node_types:
        relation_types.append({'name': 'Package_to_Document',
                               'weight': 10})
        relation_types.append({'name': 'Document_to_Directory_Node',
                               'weight': 10})
        relation_types.append({'name': 'Directory_Node_to_Directory_Node',
                               'weight': 10})
        relation_types.append({'name': 'Directory_Node_to_Topic',
                               'weight': 0.5})
    if 'Component' in node_types:
        relation_types.append({'name': 'X_to_Component',
                               'weight': 0.5})
        relation_types.append({'name': 'fake_Commponent_Tree',
                               'weight': 10})
    if 'Qualification' in node_types:
        relation_types.append({'name': 'X_to_Qualification',
                               'weight': 0.5})
        relation_types.append({'name': 'fake_Qualification_Tree',
                               'weight': 10})
    return relation_types


def get_matched_links(nodes):
    links = []
    weight = 0.1
    print('entered get_matched_links - at all!')
    other_nodes = nodes.copy()
    for node in nodes:
        print(str(node))
        other_nodes.remove(node)
        for other_node in other_nodes:
            link = {}
            if node['id'] == other_node['id'] \
               and node['type'] == 'Topic':
                link['source'] = node['graph-id'] - 1
                link['target'] = other_node['graph-id'] - 1
                link['weight'] = weight
                links.append(link)
    print('links: ' + str(links))
    return links

    """
    :return: Requested value out of memory or default.
    """
def get_memory_values(memory_data, value):
    if memory_data and memory_data.get('packages', False):
        if value == 'node_types' or value == 'sel_value_node_types':
            return memory_data['node_types']
        selected_packages = [packages[x] for x in memory_data.get('packages')]
        return [x[value] for x in selected_packages]
    elif value == 'node_types' or value == 'sel_value_node_types':
        return default_node_types
    else:
        return [packages['default'][value]]


def offset_links(links, offset):
    for link in links:
        link['source'] = link['source'] + offset
        link['target'] = link['target'] + offset
    return links

def offset_nodes(nodes, offset):
    for node in nodes:
        node['graph-id'] = node['graph-id'] + offset
    return nodes

def easy_output(label, g):
    print('\n' + label + ':')
    import pprint
    for stmt in g:
        pprint.pprint(stmt)


def intermediary_output(msg, item):
    print('\n' + msg + ':')
    import pprint
    pprint(item)
    return item


app = dash.Dash(__name__)
try:
    app.server.config.from_envvar('VISUAL_EXPLORER_FOR_IIRDS_SETTINGS')
except FileNotFoundError:
    print('WARNING: Cannot find configuration including SECURITY_KEY. Before deployment, set it up!')
app.config['suppress_callback_exceptions'] = True
# app.css.config.serve_locally = True
# app.scripts.config.serve_locally = True

# took {%css%} out and nailed own css in because css-file in /assets would not be 
# withough touching it in a browser Webdev tool and threw:
# Resource interpreted as Stylesheet but transferred with MIME type text/plain

app.index_string = '''
<!DOCTYPE html>
<html>
    <head>
        <style>
            body {
                font-family: sans-serif;
            }


            div#app-container {
                width: 100%;
            }

            div#banner {
                width: 100%;
                margin-bottom: 50px;
                border-bottom: 2px solid #1975FA;
            }

            p.app-name {
                height:40px
                margin-top: 20px;
                margin-bottom: 20px;
                margin-left: 20px;
                margin-right: 210px;
                vertical-align: middle;
                font-size: 30px;
            }

            div#iirds-compliant {
                margin-left: 12px;
                float: right;
                vertical-align: middle;
            }

            img.iirds-compliant-logo {
                vertical-align: middle;
                max-height: 40px;
            }

            div#powered-by {
                float: right;
                vertical-align: middle;
            }

            span.powered-by {
                vertical-align: middle;
                margin-right: 12px;
            }

            img.plotly-logo {
                vertical-align: middle;
                max-height: 40px;
            }

            div#main-column {
                margin-right: 430px;
                height: 600px;
            }

            div#right-column {
                width: 400px;
                float: right;
                clear: right;
                border: 1px solid #d6d6d6;
            }

            div#tabs div {
                background-color: white;
            }

            div#tabs-content {
                padding: 10px;
            }

            iframe#node-content-display {
                width: 390px;
                height: 500px;
                border: none;
            }

            div#display-store, div#config-store {
                display: none;
            }

            .Select--multi .Select-value {
                background-color: white;
                color: black;
                border: 1px solid #d6d6d6;

            .Select--multi .Select-value-icon {
                border: 1px solid #d6d6d6;
            }

        </style>
        {%metas%}
        <title>Visual Explorer for iiRDS</title>
        {%favicon%}

    </head>
    <body>
        {%app_entry%}
        <footer>
            {%config%}
            {%scripts%}
            {%renderer%}
        </footer>
    </body>
</html>
'''

app.layout = html.Div(
    id="app-container",
    children=[
        dcc.Store(id='memory', storage_type='memory'),
        html.Div(
            id="banner",
            className="banner",
            children=[
                html.Div(
                    id='iirds-compliant',
                    children=[
                        html.A(
                            className='iirds-compliant-link',
                            href='https://iirds.org/',
                            target='_blank',
                            title='iiRDS – The International Standard for intelligent information Request and Delivery.',
                            children=[
                                html.Img(
                                    src=app.get_asset_url("iiRDS_compliant_scaled.jpg"),
                                    className='iirds-compliant-logo')
                            ]
                        )
                    ]),
                html.Div(
                    id='powered-by',
                    children=[
                        html.Span('powered by ', className='powered-by'),
                        html.A(
                            className='plotly-link',
                            href='https://plotly.com/dash/',
                            target='_blank',
                            title='Dash is the most downloaded, trusted framework for building ML & data science web apps.',
                            children=[
                                html.Img(
                                    src=app.get_asset_url("plotly_logo.png"),
                                    className='plotly-logo')
                            ]
                        )
                    ]),
                html.Div(
                    id='app-name',
                    children=[html.P('Visual Explorer for iiRDS', className='app-name')])]
        ),
        html.Div(
            id="right-column",
            className="right-column",
            children=[
                dcc.Tabs(
                    id='tabs',
                    value='tab-about',
                    children=[
                        dcc.Tab(label='About', value='tab-about'),
                        dcc.Tab(label='Settings', value='tab-config'),
                        dcc.Tab(label='Content', value='tab-display')
                    ]),
                html.Div(
                    id='tabs-content'
            )]
        ),
        html.Div(
            id="main-column",
            className="main-column",
            children=[get_chart_card([])]
        ),
        html.Div(id='display-store'),
        html.Div(id='loop-breaker-container', children=[])
    ]
)


@app.callback([Output('tabs', 'value'),
               Output('display-store', 'children')],
              [Input('network-graph', 'clickData')])
def render_tab_display(clickData):
    if clickData:
        return 'tab-display', json.dumps(clickData)
    else:
        return 'tab-about', json.dumps(clickData)


@app.callback([Output('tabs-content', 'children')],
              [Input('tabs', 'value'),
               Input('display-store', 'children')],
              [State('memory', 'data')])
def render_tab_content(tab, display_store, memory_data):
    content = []
    if tab == 'tab-about':
        content = get_about_card()
    elif tab == 'tab-display':
        content = get_display_card(display_store, memory_data)
    elif tab == 'tab-config':
        content = get_config_card(memory_data)
    return content


@app.callback([Output('memory', 'data')],
              [Input('package', 'value'),
               Input('node_types', 'value')])
def build_memory(package, node_types):
    memory_data = {}
    memory_data['packages'] = package
    node_types_out = ['Package']
    if 'Document_AND_Directory_Node' in node_types:
        node_types_out.append('Document')
        node_types_out.append('Directory_Node')
    node_types_out.append('Topic')
    if 'Component_AND_Component_root' in node_types:
        node_types_out.append('Component')
        node_types_out.append('Component_root')
    if 'Qualification_AND_Qualification_root' in node_types:
        node_types_out.append('Qualification')
        node_types_out.append('Qualification_root')
    memory_data['node_types'] = node_types_out
    return [memory_data]


@app.callback([Output('main-column', 'children')],
              [Input('memory', 'data')])
def render_chart_card(memory_data):
    return [get_chart_card(memory_data)]


if __name__ == '__main__':
    # if running locally, type into browser: localhost:8000
    # using host='0.0.0.0' for deployment on public server

    app.run_server(port=8000, host='0.0.0.0',
                   dev_tools_silence_routes_logging=debug_ext)
