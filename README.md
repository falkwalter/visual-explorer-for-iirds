# Visual Explorer for iiRDS

Visual Explorer for iiRDS means browsing and understanding iiRDS live, interactively and in color.
iiRDS is a technical communication data exchange format, see https://iirds.org for details.
Based on Dash and plotly.